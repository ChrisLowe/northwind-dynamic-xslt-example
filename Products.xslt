<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
 <html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
<title>Cross Browser Compatible Parameter Passing</title>

<!-- javascript processor for dynamically loading the new xslts -->
<script language="javascript" type="text/javascript" src="processxml.js"></script>

</head>
<body>


	<!-- the contents of this div is the one that is replaced by processxml.js -->
	<div id="main_body">


	   <h2>Northwind Products</h2>
	   <table border="1">
		 <tr bgcolor="#9acd32">
		   <th>Name</th>
		   <th>Category</th>
		   <th>Quantity per Unit</th>
		   <th>Price</th>
		   <th></th>
		 </tr>
		 <xsl:for-each select="NWind/Product">

		 <tr>
		  <td><xsl:value-of select="ProductName"/></td>
		  <td><xsl:value-of select="CategoryName"/></td>
		  <td><xsl:value-of select="QuantityPerUnit"/></td>
		  <td><xsl:value-of select="UnitPrice"/></td>
		  <td>
		  <a>
			<xsl:attribute name="href">javascript:transform('<xsl:value-of select="ProductID" />');</xsl:attribute>
			 Full Details
		  </a>
		  
		  
		  </td>

		 </tr>
		 </xsl:for-each>
	   </table>
			

	</div>

 </body>
 </html>
</xsl:template></xsl:stylesheet>