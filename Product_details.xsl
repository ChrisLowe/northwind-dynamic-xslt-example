<?xml version="1.0" encoding="utf-8"?>

<!-- initially created by Adam Same (2008) and Dr Justin Brown (2009).  Reworked by Christopher Lowe (2012) -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:param name="sid" />
<xsl:param name="client" />
<xsl:template match="/">


	<h2>Product Information</h2>

		<xsl:for-each select="NWind/Product[ProductID=$sid]">
			<strong>Product ID:</strong> <xsl:value-of select="ProductID" /><hr />
			<strong>Product Name:</strong> <xsl:value-of select="ProductName" /><br />
			<strong>Product Category:</strong> <xsl:value-of select="CategoryName" /><br />
			<strong>Company Name:</strong> <xsl:value-of select="CompanyName" /><br />
			<strong>Quantity per Unit:</strong> <xsl:value-of select="QuantityPerUnit" /><br />
			<strong>Unit Price:</strong> $<xsl:value-of select="UnitPrice" /><br />
			<strong>Units in Stock:</strong> <xsl:value-of select="UnitsInStock" /><br />
			<strong>Units on Order:</strong> <xsl:value-of select="UnitsOnOrder" /><br />
			<strong>Reorder Level:</strong> <xsl:value-of select="ReorderLevel" /><br />
			<strong>Discontinued:</strong> 
			<xsl:if test="Discontinued=0">
			No
			</xsl:if>
			<xsl:if test="Discontinued=1">
			Yes
			</xsl:if>					   


			<br />

			<a href="Nwind.xml">Back</a>

		</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>